<?php
require_once 'config/init.php';
$id_event = $_GET['id_event'];
/*nampilin suggest event di halaman bawah*/
$events_random = tampilkan_event_random();

if (isset($_GET['id_event'])) {
  $event_per_id = tampilkan_event_per_id($id_event);
  while ($row = mysqli_fetch_assoc($event_per_id)) {
    $id_event = $row['id_event'];
    $nama_event = $row['nama_event'];
    $tanggal_event = $row['tanggal_event'];
    $waktu_event = $row['waktu_event'];
    $keterangan_event = $row['keterangan_event'];
    $lokasi_event = $row['lokasi_event'];
    $url_lokasi = $row['url_lokasi'];
    $kategori = $row['kategori'];
    $foto_event = $row['foto'];
  }
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Diallovite, a free event organizer made by Technosoft">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Diallovite | Event Organizer Made By Technosoft</title>
    <link rel="shortcut icon" href="assets/images/diallovite_final_latest.ico">
    <link rel="stylesheet" href="assets/css/materialize.min.css">
    <link rel="stylesheet" href="assets/css/extra-css_read-more.css">
    <link rel="stylesheet" href="assets/css/general.css">
  </head>
  <body>
    <?php require_once 'assets/components/nav/nav-home.php'; ?>
        <header>
          <img src="<?= $foto_event; ?>" alt="Foto Event">
        </header>
        <div class="mycontainer_without_padding">
          <div class="row margin-reset">
            <div class="col s12 m9 l9" id="div_share">
              <a href="#share_link" class="modal-trigger" onclick="get_url()" title="Share This Events">
                <i class="material-icons">link</i>
              </a>
              <div id="share_link" class="modal">
                <div class="modal-content padding-reset">
                  <div class="col s12 center-align" id="title_share">
                    <h5 class="margin-reset font-bold">Share This Events</h5>
                  </div>
                  <div class="col s12" id="input_share">
                    <div class="row center-align">
                      <div class="addthis_inline_share_toolbox"></div>
                    </div>
                    <input type="text" class="white margin-reset border-box" id="share_url_page" value="">
                  </div>
                  <div class="col s12" id="copy_share">
                    <a href="javascript:void(0)" id="btn_share" data-clipboard-target="#share_url_page"
                       onclick="Materialize.toast('Copied', 3000, 'rounded')">Copy Link</a>
                  </div>
                </div>
              </div>
              <a href="#!" rel="bookmark"><i class="material-icons">bookmark_border</i></a>
            </div>
            <div class="col s12 m3 l3 center-align padding-left_reset padding-right_reset" id="div_join">
              <a href="register.php?id_event=<?=$_GET['id_event']?>"
                 class="btn waves-effect waves-light col s12 radius-20">Join Event</a>
            </div>
          </div>
          <div class="row">
            <div class="col s12 m9 l9">
              <p class="font-bold font-ubuntu grey-text text-darken-4">Description</p>
              <p class="font-arial grey-text justify-align text-darken-3"><?= $keterangan_event;?></p>
              <p class="font-bold font-ubuntu grey-text text-darken-4">Categories</p>
              <div class="chip grey lighten-3">
                <a class="capitalize" href="#"><?= $kategori ;?></a>
              </div>
              <p class="font-bold font-ubuntu grey-text text-darken-4">Tag</p>
              <div class="chip grey lighten-3 text-grey text-darken-4">
                <a href="index.php?search=<?= $lokasi_event;?>">Things To Do In <?= $lokasi_event ;?></a>
              </div>
              <div class="chip grey lighten-3 text-grey text-darken-4 capitalize">
                <a href="index.php?search=<?= $kategori;?>"><?= $kategori; ?></a>
              </div>
              <div class="chip grey lighten-3 text-grey text-darken-4 capitalize">
                <a href="/duet_project">Diallovite</a>
              </div>
            </div>
            <div class="col s12 m3 l3" id="date_time_event">
              <p class="font-bolder uppercase">Date And Time</p>
              <p><?= $tanggal_event; ?></p>
              <p><?= $waktu_event; ?></p>
              <br>
              <span class="font-bolder uppercase">Location</span>
              <p><a href="index.php?search=<?= $lokasi_event;?>"><?= $lokasi_event; ?></a></p>
            </div>
          </div>
          <div class="row">
            <iframe class="col s12 no-border padding-reset" src="<?= $url_lokasi; ?>" allowfullscreen></iframe>
          </div>
          <div class="row">
            <div class="col s12"><div class="divider"></div></div>
          </div>
          <div class="section">
            <div class="row">
              <div class="col s12">
                <h4 class="center-align font-bold font-cabin grey-text text-darken-4"
                    style="font-size:23px;">Other Events May You Like</h4>
              </div>
            </div>
            <div class="row">
              <div class="col s12">
                <?php while ($row = mysqli_fetch_assoc($events_random)): ?>
                <div class="col s12 m4 l4">
                  <div class="card z-depth-1 small">
                    <div class="card-image">
                      <img src="<?= $row['foto']; ?>" class="activator" id="image">
                    </div>
                    <div class="card-content">
                      <a href="javascript:void(0);">Lokasi : <?= $row['lokasi_event']; ?></a>
                    </div>
                    <div class="card-action">
                      <a href="javascript:void(0);">
                        <?= $row['tanggal_event']; ?>
                      </a>
                      <a class="right margin-right_reset"
                         href="read_more.php?id_event=<?= $row['id_event']; ?>">Join Event</a>
                    </div>
                  </div>
                </div>
                <?php endwhile; ?>
              </div>
            </div>
          </div>
        </div>

    <?php require_once 'assets/components/footer/footer.php'; ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.7.1/clipboard.min.js"></script>
    <script src="assets/js/jquery.min.js" charset="utf-8"></script>
    <script src="assets/js/materialize.min.js" charset="utf-8"></script>
    <script>
      $(document).ready(function(){
        $('.modal').modal();
      });
    </script>
    <script>
      var clipboard = new Clipboard('#btn_share');
      clipboard.on('success', function(e) {
        console.log(e);
      });
      clipboard.on('error', function(e) {
        console.log(e);
      });

      function get_url() {
        document.getElementById('share_url_page').value = window.location.href;
      }
    </script>
    <script type="text/javascript"
            src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5a2a03500c470813"></script>
  </body>
</html>
