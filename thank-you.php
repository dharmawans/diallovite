<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Thank You</title>
    <link rel="stylesheet" href="assets/css/thank-you.css">
    <link rel="stylesheet" href="assets/css/general.css">
    <link rel="shortcut icon" href="assets/images/diallovite_final_latest.ico">
  </head>
  <body>
    <div class="container center-align">
        <img src="assets/images/thank-you.png" width="300" alt="done event" class="padding-bottom_reset" draggable="false">
        <p class="font-lobster_two">You've been added to this event .</p>
        <p class="font-lobster_two">Please check <span class="font-italic"> your email </span> to confirm the ticket .</p>
        <button class="no-outline" type="button" name="button"><a href="/duet_project" draggable="false">Browse Others Event</a></button>
    </div>
    <script src="assets/js/jquery.min.js" charset="utf-8"></script>
</html>
