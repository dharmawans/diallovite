-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 31, 2018 at 04:27 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.0.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `duet_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `email`, `password`) VALUES
(1, 'admin@admin.com', 'gakadapassword');

-- --------------------------------------------------------

--
-- Table structure for table `anggota`
--

CREATE TABLE `anggota` (
  `id_peserta` int(10) NOT NULL,
  `id_event` int(10) NOT NULL,
  `nama_peserta` varchar(100) NOT NULL,
  `email_peserta` varchar(100) NOT NULL,
  `tanggal_lahir` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `anggota`
--

INSERT INTO `anggota` (`id_peserta`, `id_event`, `nama_peserta`, `email_peserta`, `tanggal_lahir`) VALUES
(1, 18, 'nama panjang', 'email@email.com', '2017-11-20'),
(3, 18, 'jsjsk21', 'jsj@jsj.com', '2017-11-28'),
(5, 18, 'orang satu', 'orang@satu.com', '2017-11-08'),
(6, 18, 'Raihan', 'raihan@ekap.com', '2017-11-13'),
(7, 18, 'Bariq', 'bariq@dharmawan.com', '2017-11-10'),
(8, 21, 'Bariq Dharmawan', 'email@bariq.com', '2017-03-16'),
(9, 21, 'Bariq Dharmawan', 'email@bariq.com', '2017-03-16'),
(10, 26, 'Rendi Firmansyah', 'rendi@firmansyah', '2017-08-30'),
(11, 20, 'Simson', 'email@simson.com', '2017-11-08'),
(12, 21, 'Lutfiah Fachdiah', 'email@mpi.com', '2017-01-02'),
(13, 19, 'Jeyhan', 'jeyhan@email.com', '2017-12-13'),
(14, 20, 'Lutfiah', 'mpi@email.com', '2017-01-02'),
(15, 18, 'nabil akbar', 'nabil@akbar.com', '2017-12-11');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id_event` int(11) NOT NULL,
  `nama_event` varchar(50) NOT NULL,
  `waktu_event` varchar(40) NOT NULL,
  `tanggal_event` varchar(100) NOT NULL,
  `lokasi_event` text NOT NULL,
  `url_lokasi` text NOT NULL,
  `keterangan_event` text NOT NULL,
  `kategori` varchar(30) NOT NULL,
  `foto` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id_event`, `nama_event`, `waktu_event`, `tanggal_event`, `lokasi_event`, `url_lokasi`, `keterangan_event`, `kategori`, `foto`) VALUES
(18, 'event 1', '13:00 - 15:00', 'Senin, 16-03-2017', 'Jakarta Selatan', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d126907.08036897321!2d106.73203844505501!3d-6.283931254512833!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f1ec2422b0b3%3A0x39a0d0fe47404d02!2sJakarta+Selatan%2C+Kota+Jakarta+Selatan%2C+Daerah+Khusus+Ibukota+Jakarta!5e0!3m2!1sid!2sid!4v1515086785785', 'kasfkuafakg\r\nasfkaufaekgiwruigiwruiwrg', 'music', 'assets/images/event_first.jpg'),
(19, 'event 2', '13:00 - 15:00', 'Selasa, 16-03-2017', 'Jakarta Timur', '', 'kjsajfjkjk\r\nsafhhhehehe', 'technology', 'assets/images/event2.png'),
(20, 'Event 3', '13:00 - 15:00', 'Rabu, 16-03-2017', 'Jakarta Utara', '', 'jkasfaeuhf\r\naieuuigiwuiui', 'arts', 'assets/images/event3.jpg'),
(21, 'event 4', '13:00 - 15:00', 'Kamis, 16-03-2017', 'Jakarta Pusat', '', 'kjasfuiequi\r\nsafhjhhjhjh', 'technology', 'assets/images/event4.jpg'),
(22, 'Lks', '13:00 - 15:00', 'Jumat, 16-03-2017', 'SMKN 10 Jakarta', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.060264376119!2d106.86752601437074!3d-6.255791462989727!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f301ea1c2901%3A0xfc9931c7b0225846!2sSMK+Negeri+10+Jakarta!5e0!3m2!1sid!2sid!4v1515087058588', 'jaskhfjgwlrhkdjfskdhgsag\r\nsdgjksdhgksjdgjskhgrjwhgkwrhgkrw', 'religion', 'assets/images/ngasoy.jpg'),
(23, 'ramadhan', '13:00 - 15:00', '2017-11-17', 'SMKN 10', '', 'ini event ramadhan', 'religion', 'assets/images/background1.jpg'),
(24, 'event lainnya', '13:00 - 15:00', '2017-12-06', 'Rumah Gue', '', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut\r\n            labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris\r\n            nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\n            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,\r\n            sunt in culpa qui officia deserunt mollit anim id est laborum.qui officia deserunt mollit anim id\r\n            est laborum.culpa qui officia deserunt mollit anim id est laborum.qui officia deserunt mollit anim id.', 'music', 'assets/images/ngasoy.jpg'),
(25, 'ngasoy juga', '13:00 - 15:00', '2017-10-23', 'Rumah dia', '', 'sakfjhasfkajkfhakjhgkjg\r\nwgdkjghjkdahgajkhgkajghk', 'music', 'assets/images/ngasoy.jpg'),
(26, 'ngasoyagain', '13:00 - 15:00', '2017-10-02', 'manaaja', '', 'sakfjhasfkajkfhakjhgkjg\r\nwgdkjghjkdahgajkhgkajghk', 'music', 'assets/images/ngasoy.jpg'),
(27, 'lagi ngasoy', '13:00 - 15:00', '2017-10-31', 'pulau', '', 'bnjbjasgfjasfsagfagfjsabfasfhasfhfbfajbfjasbfjbsafjasbfjasbh', 'music', 'assets/images/ngasoy.jpg'),
(32, 'PMR', '', '2017-11-22', 'Gedung Balai Kota no.14, Jakarta Selatan, Indonesia', '', 'jsafkhsakfjhaskjfhaskjhfaskjfhaskfhaksjfhaksfhakjsfhakjhf ddjak ghjfasfaskjfhasjhfaskjfhsakjfh jkafjasfsjkgfhgfsrgsr hjgdjfhfgsahfashfjfggfgfj hjfgjagfhjdfjdgfhkfkashfkjhfkjahfjkahf asjkhfkjashfkajshfkajshfkajshfaksjfhaskjfhak', 'arts', 'assets/images/pmr.jpg'),
(33, 'Sang Idola Cinta', '19:00 - 24:00', '2017-11-30', 'Jakarta Pusat', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.66116838073!2d106.82264942020498!3d-6.176094362544124!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f5d2e764b12d%3A0x3d2ad6e1e0e9bcc8!2sMonumen+Nasional!5e0!3m2!1sid!2sid!4v1515086987738', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse consectetur ultrices lacinia. Sed eget mollis lectus. Aliquam erat volutpat. Donec velit tellus, sagittis id placerat sed, porta in sapien. Ut sodales vitae nisl eu ornare. Nullam sed tristique est. Ut eu sem sed ipsum semper finibus. Praesent ante sem, faucibus non blandit vel, pretium at elit. Nullam quis odio molestie, venenatis lectus sit amet, euismod elit. Morbi quis faucibus tortor. Aliquam vel dolor non nisl vestibulum cursus. Maecenas vel neque sollicitudin, mollis dolor at, molestie neque. Cras ut egestas metus. Pellentesque eget lacus erat. In hac habitasse platea dictumst. Nulla id orci eu leo vulputate faucibus.\r\n\r\nEtiam scelerisque commodo odio, id euismod elit imperdiet sit amet. Morbi scelerisque nulla ac iaculis lobortis. Proin sapien lorem, cursus ut vulputate ut, facilisis ac orci. Donec sit amet sapien eros. Sed iaculis urna quis diam sagittis semper quis ut dui. Suspendisse rutrum efficitur ligula vitae consectetur. Etiam ultricies condimentum mauris, a consequat tellus ultrices sed. Vestibulum viverra eros est. Etiam facilisis dapibus sodales. Integer ultrices iaculis efficitur. Sed laoreet viverra lectus ac varius. Suspendisse potenti.\r\n\r\nUt nec diam non ante vehicula congue. In vehicula in tortor id iaculis. Donec sed massa id turpis molestie suscipit eu et sem. Aenean dignissim, leo non pretium aliquet, purus ipsum efficitur velit, non tincidunt purus lorem ut quam. Nullam quis neque ac dolor accumsan efficitur ut id est. Etiam interdum diam nibh, nec iaculis turpis pulvinar sit amet. Sed eget tempus magna, vel ornare diam. Nunc rutrum enim sit amet libero pharetra, nec hendrerit felis semper. Etiam vel euismod mi. Etiam non iaculis risus, nec accumsan libero. Phasellus nec purus posuere, tempor massa sit amet, dapibus arcu.\r\n\r\nSed ut neque ut tellus sollicitudin volutpat non non dolor. Sed tellus leo, ullamcorper at dui ut, tincidunt convallis quam. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec non vulputate risus. Praesent at dictum leo. Sed ac tempor lorem. Morbi rhoncus nec lorem eu pretium. Praesent consectetur vitae metus et tincidunt. Integer non tempor ligula. Duis accumsan mauris eu laoreet efficitur. Curabitur ultrices vel sapien in scelerisque. In et nisi magna. Donec egestas semper finibus.\r\n\r\nNam pretium felis nec sem posuere varius. Sed auctor fermentum tortor id vehicula. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed faucibus lacus diam, sit amet scelerisque ligula consequat a. Ut ac viverra eros. Nullam pharetra imperdiet nisi non porttitor. Proin tempor purus tortor. Ut imperdiet sem diam, id tincidunt sem scelerisque eget. Nulla eu magna auctor, condimentum metus eget, dignissim augue. Mauris cursus erat eu tortor sollicitudin, ac aliquam lectus commodo.', 'religion', 'assets/images/sang_idola_cinta.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `saran`
--

CREATE TABLE `saran` (
  `id` int(11) NOT NULL,
  `nama_pengirim` varchar(50) NOT NULL,
  `email_pengirim` varchar(100) NOT NULL,
  `pesan_pengirim` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `saran`
--

INSERT INTO `saran` (`id`, `nama_pengirim`, `email_pengirim`, `pesan_pengirim`) VALUES
(6, 'Shabrina Fitri', 'shabrina@fitri.com', 'sakjfkeg'),
(8, 'Mega Tri', 'mega@tri.com', 'kasdjhekrm'),
(12, 'Aditya Rifai', 'aditya@rifai.com', 'asjkrjghtr'),
(18, 'Nabil Akbar', 'nabil@akbar.com', 'kjasfkjwre'),
(31, 'Lutfiah Fachdiah', 'mpi@mpi.com', 'sajkhfkmngr'),
(40, 'Nama', 'email@nama.com', 'askjhfkgkjdms'),
(46, 'Isa Goutama', 'isa@goutama', 'asjfhjmgdjhj'),
(49, 'Goutama Isa', 'goutama@isa.com', 'kasfajdhe'),
(50, 'Kak Anas', 'kak@anas.com', 'jksahfjhw'),
(53, '', 'safa@sak', '\r\n\r\n'),
(54, 'sa', '', 'ssas'),
(55, 'nama orang', '', 'ini pesan'),
(56, 'jka', '', 'as'),
(57, 'asf', '', 'as'),
(58, 'Momo', '', 'asfafjkasfa'),
(59, 'asfjk', '', 'kjasfkashfsa'),
(60, 'Isa Goutama', '', 'saran baru'),
(61, 'bariq', '', 'jksafkjae'),
(62, 'fikri', '', 'asfjksafhkejq'),
(67, '', '', ''),
(68, '', '', ''),
(69, '', '', ''),
(70, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `slideshow`
--

CREATE TABLE `slideshow` (
  `id_slideshow` int(11) NOT NULL,
  `gambar_slideshow` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slideshow`
--

INSERT INTO `slideshow` (`id_slideshow`, `gambar_slideshow`) VALUES
(1, 'assets/images/androidify.jpg'),
(2, 'https://i.ytimg.com/vi/9y-mCOY4wI8/maxresdefault.jpg'),
(3, 'assets/images/slideshow3.jpg'),
(4, 'assets/images/2018.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `anggota`
--
ALTER TABLE `anggota`
  ADD PRIMARY KEY (`id_peserta`),
  ADD KEY `relasi` (`id_event`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id_event`);

--
-- Indexes for table `saran`
--
ALTER TABLE `saran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slideshow`
--
ALTER TABLE `slideshow`
  ADD PRIMARY KEY (`id_slideshow`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `anggota`
--
ALTER TABLE `anggota`
  MODIFY `id_peserta` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id_event` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `saran`
--
ALTER TABLE `saran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `slideshow`
--
ALTER TABLE `slideshow`
  MODIFY `id_slideshow` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `anggota`
--
ALTER TABLE `anggota`
  ADD CONSTRAINT `relasi` FOREIGN KEY (`id_event`) REFERENCES `events` (`id_event`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
