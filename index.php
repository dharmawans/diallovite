<?php
require_once 'config/init.php';
$events = tampilkan_event();
$slideshow = tampilkan_slideshow();
//function search
if (isset($_GET['search']))
{
	$cari = $_GET['search'];
	$events = hasil_cari($cari);
}

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Diallovite, a free event organizer made by Technosoft">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Diallovite | Event Organizer Made By Technosoft</title>
  <link rel="shortcut icon" href="assets/images/diallovite_final_latest.ico">
	<link rel="stylesheet" href="assets/css/materialize.min.css">
  <link rel="stylesheet" href="assets/css/extra-css.css">
	<link rel="stylesheet" href="assets/css/general.css">
</head>
<body>
	<?php require_once 'assets/components/nav/nav-home.php'; ?>
	<section class="slider" style="z-index: -1;">
	  <ul class="slides">
			<?php while ($row = mysqli_fetch_assoc($slideshow)): ?>
			<li>
				<img src="<?= $row['gambar_slideshow'];?>">
			</li>
		<?php endwhile; ?>
		</ul>
	</section>
	<!-- The .scrollTo is use for smooth scrool -->
	<section class="search_box z-depth-1 center-align hide-on-med-and-down">
		<h4 class="font-cabin">What are you looking for?</h4>
		<form method="get">
			<input type="search" name="search" class="browser-default font-cabin no-outline font-bold"
						 placeholder="Search Location or Category" autocomplete="off">
		</form>
	</section>
	<main class="mycontainer_without_padding">
		<section class="hide-on-large-only" id="search_on_med_and_down">
			<div class="row center-align">
				<form method="get">
					<input type="search" name="search" class="browser-default font-cabin no-outline font-bold"
								 placeholder="Search Location or Category" autocomplete="off">
				</form>
			</div>
		</section>
		<article class="row">
	  	<?php while($row = mysqli_fetch_assoc($events)) {?>
	  	<section class="col s12 m6 l4">
	    	<p class="capitalize font-bold font-cabin midnight-blue_text" id="title_event"><?php echo $row['nama_event'] ?>
					<span class="right"><?php echo $row['kategori'] ?></span>
				</p>
	      <div class="card z-depth-1 sticky-action">
	        <div class="card-image waves-effect waves-block waves-light">
	          <img src="<?php echo $row['foto'] ?>" class="activator">
	        </div>
	        <div class="card-content">
	          <a href="javascript:void(0);">
							<?php echo mb_strimwidth($row['lokasi_event'], 0, 13, "...") ?>
						</a>
	          <a href="javascript:void(0);" class="activator">
							<i class="material-icons right">more_vert</i>
						</a>
		      </div>
		      <div class="card-reveal" id="card-reveal">
		        <span class="card-title grey-text text-darken-4">
		         	<?php echo $row['nama_event'] ?><i class="material-icons right">close</i>
		         </span>
		         <p><?php echo mb_strimwidth($row['keterangan_event'], 0, 220, "...") ?></p>
						<a href="read_more?id_event=<?php echo $row['id_event'] ?>">Read More</a>
	        </div>
	        <div class="card-action">
	          	<a class="margin-reset" href="javascript:void(0);">
	          		<?php echo $row['tanggal_event'] ?>
	          	</a>
							<a class="right margin-reset" href="read_more?id_event=<?php echo $row['id_event'] ?>">
								Join Event
							</a>
	      	</div>
	    	</div>
			</section>
		<?php } ?>
	  </article>
		<div class="row">
			<div class="see_more col s12 center-align">
				<!-- <a href="javascript:void(0);" id="see_more">See More Event</a> -->
				<ul class="pagination">
					<li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
					<li class="active coral-blue waves-effect"><a href="#!">1</a></li>
					<li class="waves-effect"><a href="#!">2</a></li>
					<li class="waves-effect"><a href="#!">3</a></li>
					<li class="waves-effect"><a href="#!">4</a></li>
					<li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="row">
				<div class="col s12">
					<h4 class="center-align grey-text darken-3 bold flow-text">Browse Events By Categories</h4>
				</div>
			</div>
				<div class="browse_categories">
					<div class="row">
							<div class="padding-reset container_category col s12 m8">
								<a href="?search=technology" class="link_categories">
								<div class="category_image">
									<p class="font-bold white-text">Technology</p>
									<p class="white-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
										incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
								</div>
								</a>
							</div>
							<div class="padding-reset container_category col s12 m4">
								<a href="?search=music">
								<div class="category_image">
									<p class="font-bold white-text">Music</p>
									<p class="white-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
								</div>
								</a>
							</div>
					</div>
					<div class="row">
							<div class="container_category col s12 m4 padding-reset">
								<a href="?search=arts" class="link_categories">
								<div class="category_image">
									<p class="font-bold white-text">Arts</p>
									<p class="white-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
										incididunt ut labore</p>
									</div>
								</a>
							</div>
							<div class="padding-reset container_category col s12 m8">
								<a href="?search=class+room" class="link_categories">
								<div class="category_image">
									<p class="font-bold white-text">Class Room</p>
									<p class="white-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
										incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
								</div>
								</a>
							</div>
					</div>
					<div class="row">
					<div class="padding-reset container_category col s12 m6">
								<a href="?search=religion" class="link_categories">
								<div class="category_image">
									<p class="font-bold white-text">Religion</p>
									<p class="white-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
										incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
								</div>
								</a>
					</div>
					<div class="padding-reset container_category col s12 m6">
						<a href="?search=sports" class="link_categories">
							<div class="category_image">
								<p class="font-bold white-text">Sports</p>
								<p class="white-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
									incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</main>
	<?php require_once 'assets/components/footer/footer.php'; ?>
	<script src="assets/js/jquery.min.js" charset="utf-8"></script>
  <script src="assets/js/materialize.min.js" charset="utf-8"></script>
	<script src="assets/js/home_page.js" charset="utf-8"></script>
</body>
</html>
