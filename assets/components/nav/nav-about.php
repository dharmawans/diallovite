<!-- navbar desktop -->
<header class="hide-on-med-and-down navbar-fixed">
  <nav class="white" role="navigation">
    <div class="nav-wrapper mycontainer_without_padding">
      <a href="/duet_project" class="brand-logo cool-green_text margin-reset font-lobster"><b>Diallovite</b></a>
      <ul class="right hide-on-med-and-down">
        <li><a href="/duet_project" class="btn waves-light waves-effect wet-asphalt radius-20 margin-right_reset margin-left_reset">Home</a></li>
        <li><a href="kontak" class="btn waves-light waves-effect pumpkin-orange radius-20">Contact Us</a></li>
      </ul>
      <ul id="nav-mobile" class="side-nav white">
        <li><a href="/duet_project">Home</a></li>
        <li><a href="kontak">Contact Us</a></li>
      </ul>
      <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
    </div>
  </nav>
</header>

<!-- navbar tablet dan handphone -->
<header class="hide-on-large-only">
<nav class="white" role="navigation">
  <div class="nav-wrapper mycontainer_without_padding">
    <a href="/duet_project" class="brand-logo cool-green_text margin-reset font-lobster"><b>Diallovite</b></a>
    <ul class="right hide-on-med-and-down">
      <li><a href="/duet_project" class="btn waves-light waves-effect wet-asphalt radius-20">Home</a></li>
      <li><a href="kontak" class="btn waves-light waves-effect pumpkin-orange radius-20">Contact Us</a></li>
    </ul>
    <ul id="nav-mobile" class="side-nav white">
      <li><a href="/duet_project">Home</a></li>
      <li><a href="kontak">Contact Us</a></li>
    </ul>
    <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
  </div>
</nav>
</header>
