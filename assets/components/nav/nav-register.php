<!-- navbar desktop -->
<header class="hide-on-med-and-down navbar-fixed">
  <nav class="white" id="nav">
      <div class="nav-wrapper mycontainer_without_padding">
          <a href="/duet_project" class="brand-logo cool-green_text margin-reset font-lobster"><b>Diallovite</b></a>
          <a href="#" data-activates="mobile-demo" class="button-collapse">
              <i class="material-icons" style="color:black">menu</i>
          </a>
          <ul class="right hide-on-med-and-down">
              <li><a href="/duet_project" class="btn waves-light waves-effect pumpkin-orange radius-20">Home</a></li>
              <li><a href="about-us" class="btn btnAbout waves-light waves-effect wet-asphalt radius-20">About</a></li>
          </ul>
          <ul class="side-nav white" id="mobile-demo">
              <li><a href="/duet_project" class="waves-light waves-effect">Home</a></li>
              <li><a href="about-us" class="waves-light waves-effect">About</a></li>
          </ul>
      </div>
  </nav>
</header>

<!-- navbar tablet dan handphone -->
<header class="hide-on-large-only">
  <nav class="white" id="nav">
      <div class="nav-wrapper mycontainer_without_padding">
          <a href="/duet_project" class="brand-logo cool-green_text margin-reset font-lobster"><b>Diallovite</b></a>
          <a href="#" data-activates="mobile-demo" class="button-collapse">
              <i class="material-icons" style="color:black">menu</i>
          </a>
          <ul class="right hide-on-med-and-down">
              <li><a href="/duet_project" class="btn waves-light waves-effect pumpkin-orange radius-20">Home</a></li>
              <li><a href="about-us" class="btn btnAbout waves-light waves-effect wet-asphalt radius-20">About</a></li>
          </ul>
          <ul class="side-nav white" id="mobile-demo">
              <li><a href="/duet_project" class="waves-light waves-effect">Home</a></li>
              <li><a href="about-us" class="waves-light waves-effect">About</a></li>
          </ul>
      </div>
  </nav>
</header>
