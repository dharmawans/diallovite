<!-- navbar desktop -->
  <nav class="white">
      <div class="nav-wrapper mycontainer_without_padding">
          <a href="/duet_project" class="brand-logo cool-green_text margin-reset font-lobster"><b>Diallovite</b></a>
          <a href="#" data-activates="mobile-demo" class="button-collapse">
              <i class="material-icons" style="color:black">menu</i>
          </a>
          <ul class="right">
              <li>
                <a href="kontak" class="btn waves-light waves-effect pumpkin-orange radius-20" id="btnJoin">
                  Contact Us
                </a>
              </li>
              <li>
                <a href="about-us" class="btn btnAbout waves-light waves-effect cool-green radius-20 margin-right_reset margin-left_reset" id="btnAbout">
                  About
                </a>
              </li>
          </ul>
      </div>
  </nav>
