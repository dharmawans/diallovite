<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<style media="screen">
	div.footer-right > a {
		padding-top: 10px !important;
	}
</style>
<footer class="footer-distributed">
	<div class="footer-right">
		<a href="#"><i class="fa fa-facebook"></i></a>
		<a href="#"><i class="fa fa-twitter"></i></a>
		<a href="#"><i class="fa fa-linkedin"></i></a>
		<a href="#"><i class="fa fa-github"></i></a>
	</div>
	<div class="footer-left">
		<p>PT. Diallovite Technosoft &copy; <?= date("Y"); ?> | All Rights Reserved.</p>
	</div>
</footer>
