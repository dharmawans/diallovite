<style media="screen">
  div.hide-on-med-and-up ul li {
    display: inline-block;
  }
  div.hide-on-med-and-up ul li:not(:last-child) {
    margin-right:17px;
  }
</style>
<footer class="page-footer wet-asphalt">
  <div class="mycontainer_without_padding">
    <div class="row">
      <div class="col l6 m6 s12">
        <h5 class="white-text">Our Mission</h5>
        <p class="grey-text text-lighten-4">We trying the best for prepare your event, and you just waiting for your event.</p>
      </div>
      <!-- social media on small device -->
      <div class="hide-on-med-and-up">
        <div class="col s12">
          <h5 class="white-text">Connect With Us</h5>
          <ul>
            <li><a class="grey-text text-lighten-3" href="#!"><img src="assets/images/svg/linkedin.svg" alt="Our Linkedin" style="height:30px"></a></li>
            <li><a class="grey-text text-lighten-3" href="#!"><img src="assets/images/svg/google-plus.svg" alt="Our Google +" style="height:30px"></a></li>
            <li><a class="grey-text text-lighten-3" href="#!"><img src="assets/images/svg/instagram.svg" alt="Our Instagram" style="height:30px"></a></li>
            <li><a class="grey-text text-lighten-3" href="#!"><img src="assets/images/svg/twitter.svg" alt="Our Twitter" style="height:30px"></a></li>
          </ul>
        </div>
      </div>
      <!-- end of social media on small device -->
      <!-- social media on medium and up device -->
      <div class="right-align hide-on-small-only">
        <div class="col l6 m6 s12">
          <h5 class="white-text">Connect With Us</h5>
          <ul>
            <li style="display: inline-block;margin-right:17px"><a class="grey-text text-lighten-3" href="#!"><img src="assets/images/svg/linkedin.svg" alt="Our Linkedin" style="height:30px"></a></li>
            <li style="display: inline-block;margin-right:17px"><a class="grey-text text-lighten-3" href="#!"><img src="assets/images/svg/google-plus.svg" alt="Our Google +" style="height:30px"></a></li>
            <li style="display: inline-block;margin-right:17px"><a class="grey-text text-lighten-3" href="#!"><img src="assets/images/svg/instagram.svg" alt="Our Instagram" style="height:30px"></a></li>
            <li style="display: inline-block"><a class="grey-text text-lighten-3" href="#!"><img src="assets/images/svg/twitter.svg" alt="Our Twitter" style="height:30px"></a></li>
          </ul>
        </div>
      </div>
      <!-- end of social media on and up device -->
    </div>
  </div>
  <div class="footer-copyright midnight-blue">
    <div class="mycontainer_without_padding">
      <!-- copyright on medium and up device -->
      <div class="hide-on-small-only">
        <div class="col m6 l6 left">Copyright &copy; <?= date("Y"); ?> Diallovite | All Rights Reserved.</div>
        <div class="col m6 l6 right">Wanna Create an event? <a class="grey-text text-lighten-4" href="kontak.php">Contact Us</a></div>
      </div>
      <!-- end of copyright on medium and up device -->
      <!-- copyright on small device -->
      <div class="hide-on-med-and-up">
        <div class="row">
          <div class="col s12">Copyright &copy; <?= date("Y"); ?> Diallovite | All Rights Reserved.</div>
          <div class="col s12">Wanna Create an event? <a class="grey-text text-lighten-4" href="kontak.php">Contact Us</a></div>
        </div>
      </div>
      <!-- end of copyright on small device -->
    </div>
  </div>
</footer>
