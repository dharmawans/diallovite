$(document).ready(function() {
  $(".sidebarBtn").click(function() {
    $(".sidebar").toggleClass("active");
    $("footer div.sosmed").toggleClass("sosmed_active");
    $("main").toggleClass("main_active");
  });

  var url = location.pathname;
  if (url.indexOf('home') > -1) {
    $("nav div.tools_left h3").text("Homepage");
  }
  else if (url.indexOf('create') > -1) {
    $("nav div.tools_left h3").text("Create Event");
  }
  else if (url.indexOf('list') > -1) {
    $("nav div.tools_left h3").text("List Events");
  }
  else if (url.indexOf('peserta') > -1) {
    $("nav div.tools_left h3").text("Peserta Events");
  }

  $("div.tools_right input[type='search']").click(function() {
    $(".search_btn").addClass("search_on");
    $("nav div.tools_left h3").css("visibility", "hidden");
  });

  $(document).on('click', function(event) {
  if (event.target.id !== 'submitButton' && event.target.id !== "search") {
    $(".search_btn").removeClass("search_on");
    $("nav div.tools_left h3").css("visibility", "visible");
  }
  else {
    $(".search_btn").addClass("search_on");
  }
  });

  $("#file").on('change', function(e) {
    var files = $(this)[0].files;
    if (files.length >= 2) {
      $("#label_span").text(files.length + "file ready to upload");
    }
    else {
      var filename = e.target.value.split('\\').pop();
      $("#label_span").text(filename);
    }
  });
});
