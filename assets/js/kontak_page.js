$(document).ready(function () {
  $(".scrollTo").on('click', function (e) {
      e.preventDefault();
      var target = $(this).attr('href');
      $('html, body').animate({
          scrollTop: ($(target).offset().top)
      }, 2000);
  });
});

(function (exports) {
function valOrFunction(val, ctx, args) {
  if (typeof val == "function") {
      return val.apply(ctx, args);
  }
  else {
      return val;
  }
}

function InvalidInputHelper(input, options) {
input.setCustomValidity(valOrFunction(options.defaultText, window, [input]));

function changeOrInput() {
    if (input.value == "") {
        input.setCustomValidity(valOrFunction(options.emptyText, window, [input]));
    } else {
        input.setCustomValidity("");
    }
}

function invalid() {
    if (input.value == "") {
        input.setCustomValidity(valOrFunction(options.emptyText, window, [input]));
    } else {
       input.setCustomValidity(valOrFunction(options.invalidText, window, [input]));
    }
}

input.addEventListener("change", changeOrInput);
input.addEventListener("input", changeOrInput);
input.addEventListener("invalid", invalid);
}
exports.InvalidInputHelper = InvalidInputHelper;
})(window);

InvalidInputHelper(document.getElementById("username"), {
emptyText: "Please fill out this field",
invalidText: function (input) {
return 'Please input only letters';
}
});
