<?php
require_once 'config/init.php';
if (empty($_GET['id_event'])) {
  header('Location: /duet_project');
}
$events = tampilkan_event();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="assets/images/diallovite_final_latest.ico">
    <link rel="stylesheet" href="assets/css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/extra-css_register.css">
    <title>Register Page | Diallovite</title>
    <link rel="stylesheet" href="assets/css/general.css">
  </head>
  <body>
    <?php require_once 'assets/components/nav/nav-register.php'; ?>
    <div class="mycontainer">
      <div class="row">
        <div class="col s12 m12 padding-reset">
            <div class="row">
              <form class="col s12 m12 l12 padding-reset" action="prosesregister.php" method="post">
                <div class="row">
                  <input type="hidden" name="id_event" value="<?=$_GET['id_event']?>">
                  <div class="input-field col s12 m12 l12">
                    <input id="first_name" type="text" name="nama_peserta" class="validate"
                           pattern="[a-zA-Z ]{1,100}" required title="Pleas Input Your Real Name">
                    <label for="first_name">Nama Panjang</label>
                  </div>
                  <div class="row">
                    <div class="input-field col s12 m12 l12">
                      <input id="email" type="email" name="email_peserta" class="validate" required>
                      <label for="email">Email</label>
                    </div>
                  </div>
                  <div class="col s12 m12 l12">
                    <input id="tl" type="text" class="datepicker" name="tanggal_lahir"
                           placeholder="Tanggal Lahir">
                  </div>
                </div>
                  <div class="col s12 m3 l3">
                    <button id="button_submit" class="btn waves-effect waves-light col s12 m12 l12 light-blue darken-3"
                            type="submit" name="submit"> Submit
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
      </div>
    <?php require_once 'assets/components/footer/footer.php'; ?>
    <script src="assets/js/jquery.min.js" charset="utf-8"></script>
    <script src="assets/js/materialize.min.js" charset="utf-8"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="assets/js/register_page.js" charset="utf-8"></script>
  </body>
</html>
