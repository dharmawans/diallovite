<?php require_once 'config/init.php'; ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Kontak Kami</title>
    <link rel="shortcut icon" href="assets/images/diallovite_final_latest.ico">
    <link rel="stylesheet" href="assets/css/kontak.css">
  	<link rel="stylesheet" href="assets/css/footer.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body>
    <section class="navigation">
      <div class="nav-container">
        <div class="brand">
          <a href="/duet_project">Diallovite</a>
        </div>
        <nav>
          <div class="nav-mobile">
            <a id="nav-toggle" href="#!">
              <span></span>
            </a>
          </div>
          <ul class="nav-list">
            <li>
              <a href="/duet_project">Home</a>
            </li>
            <li>
              <a href="about-us">About</a>
            </li>
          </ul>
        </nav>
      </div>
    </section>
      <header class="header-background">
        <div class="header-caption">
          <h2>Diallovite</h2>
          <hr>
          <p>We made you search an event even better</p>
        </div>
      </header>
      <section class="to-content">
        <a class="scrollTo" href="#slogannya">
          <button type="button">
            <i class="material-icons">keyboard_arrow_down</i>
          </button>
        </a>
      </section>
      <section class="slogan">
        <blockquote id="slogannya">
          <p>Wanna create an event? <br> or you want to give some advice to us? Use the form below </p>
        </blockquote>
      </section>
      <main class="containernya" id="containernya">
        <form action="proseskontak.php" method="post" id="form_saran">
          <input type="text" name="name" placeholder="Your Name" id="username" pattern="[a-zA-Z ]{2,20}"
                 onkeypress="return event.keyCode!=13" title="Input Your Name, only letters allowed">
          <input type="email" name="email" placeholder="Your Email" onkeypress="return event.keyCode!=13"
                 title="Input Your Email">
          <p><textarea name="content" rows="18" placeholder="Your Message" title="Give Your Advice To Us" minlength="20"></textarea></p>
          <button type="submit" id="submit_kontak" name="submit" onclick="swal();">Send a Message
            <i class="material-icons" style="font-size:20px;vertical-align:middle;">send</i>
          </button>
        </form>
      </main>
      <?php require_once 'assets/components/footer/footer-new.php'; ?>
      <script src="assets/js/jquery.min.js"></script>
      <script src="assets/js/kontak_page.js" charset="utf-8"></script>
  </body>
</html>
