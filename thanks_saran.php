<?php
require_once 'config/init.php';
// $id_event = $_GET['id_event'];
$events_random = tampilkan_event_random();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Thank You</title>
    <link rel="stylesheet" href="assets/css/general.css">
    <link rel="stylesheet" href="assets/css/thanks_saran.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="shortcut icon" href="assets/images/diallovite_final_latest.ico">
    <link rel="stylesheet" href="assets/css/footer.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body>
    <header>
      <section>
        <h5 class="white-text">
          Thanks For Your Advice
        </h5>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
          labore et dolore magna aliqua. Ut enim.</p>
      </section>
      <section>
        <img src="assets/images/macbook.png" alt="">
      </section>
    </header>
    <main class="mycontainer_without_padding">
      <div class="our_events">
        <h2 class="margin-reset">Our Events</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
          labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
          nisi ut aliquip ex ea commodo consequat.</p>
      </div>
      <?php while ($row = mysqli_fetch_assoc($events_random)): ?>
      <div class="outside_card">
        <p>Nama Event</p>
        <div class="card">
          <div class="image_events">
            <img src="<?= $row['foto']; ?>" alt="">
          </div>
          <div class="location_events">
            <span class="capitalize"><?= mb_strimwidth($row['lokasi_event'], 0, 13, "..."); ?></span>
            <i class="material-icons right">more_vert</i>
          </div>
          <div class="link_events">
            <a href="javascript:void(0);"><?= $row['tanggal_event']; ?></a>
            <a href="read_more.php?id_event=<?= $row['id_event']; ?>" class="right">Join Events</a>
          </div>
        </div>
      </div>
      <?php endwhile; ?>
    </main>
    <?php require 'assets/components/footer/footer-new.php'; ?>
  </body>
</html>
