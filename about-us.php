<?php require_once 'config/init.php'; ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>About us | Diallovite</title>
  <link rel="shortcut icon" href="assets/images/diallovite_final_latest.ico">
  <link rel="stylesheet" href="assets/css/materialize.min.css">
  <link rel="stylesheet" href="assets/css/extra-css_about.css">
  <link href="assets/css/style.css" rel="stylesheet">
  <link rel="stylesheet" href="assets/css/general.css">
</head>
<body>
  <?php require_once 'assets/components/nav/nav-about.php'; ?>
  <div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
      <div class="mycontainer_without_padding">
        <div class="div-space"></div>
        <h1 id="header_slogan" class="center-align font-bolder">Love your time</h1>
        <hr class="center-align">
        <p id="paragraph_slogan" class="center-align">Speed up your wish</p>
        <br><br>
      </div>
    </div>
    <div class="parallax">
      <img src="assets/images/background1.jpg" alt="Unsplashed background img 1">
    </div>
  </div>
  <div class="mycontainer_without_padding">
    <div class="section">
      <h5 class="center-align font-cabin font-bolder">Who We Are</h5>
      <div id="who-us">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt
          ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
          laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
          voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat
          non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.mollit
          anim id est laborum.est laborum.est laborum.est laborum.laborum.
        </p>
      </div>
    </div>
  </div>

  <div class="hide-on-small-only">
    <br><br>
  </div>
  <div class="row">
    <div class="center-align">
        <div class="col s12 m6">
            <img src="assets/images/people1.jpg" class="responsive-img" style="border-radius:5px;">
            <p class="font-cabin font-bolder"><b>Bariq Dharmawan</b></p>
            <span class="font-italic">App Developement</span>
        </div>
        <div class="col s12 m6">
          <img src="assets/images/people2.jpg" class="responsive-img" style="border-radius:5px;">
          <p class="font-cabin font-italic"><b>Abang Abangan</b></p>
          <span class="font-italic">App Developement</span>
        </div>
      </div>
      <div class="mycontainer_without_padding">
        <div class="section">
          <div class="col s12 m12">
            <div class="center-align row">
              <h5 class="font-cabin font-bolder">Our Location</h5>
              <iframe class="col s12 no-border padding-reset" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.7149254032097!2d106.8301513143699!3d-6.168914662171401!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f5ce68b5e01d%3A0xcafaf042d5840c6c!2sMasjid+Istiqlal!5e0!3m2!1sid!2sid!4v1512306666371"
                 allowfullscreen>
              </iframe>
            </div>
            <address>
              <blockquote>
                Masjid Istiqlal, Jakarta Pusat, Indonesia
              </blockquote>
            </address>
          </div>
        </div>
      </div>
    </div>
  <?php require_once 'assets/components/footer/footer.php'; ?>
  <script src="assets/js/jquery.min.js" charset="utf-8"></script>
  <script src="assets/js/materialize.min.js" charset="utf-8"></script>
  <script src="assets/js/init.js"></script>
</body>
</html>
