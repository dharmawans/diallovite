<?php
require_once '../config/init.php';
$events = tampilkan_event_for_admin();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Diallovite, a free event organizer made by Technosoft">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="tools/admin.css">
    <link href='http://fonts.googleapis.com/css?family=Ubuntu:bold' rel='stylesheet'>
  </head>
  <body>
    <section class="warning_on_smarphone">
      <p>Admin content cannot be open on smartphone</p>
    </section>
    <?php require_once 'components/sidebar.php'; ?>
    <?php require_once 'components/nav.php'; ?>
    <main>
      <div class="container">
        <table>
          <thead>
            <tr>
              <th>Nama event</th>
              <th>Kategori event</th>
              <th>Keterangan event</th>
              <th>Waktu event</th>
              <th>Lokasi event</th>
              <th>Cover</th>
            </tr>
          </thead>
          <tbody>
            <?php while($row = mysqli_fetch_assoc($events)) {?>
            <tr>
              <td><?php echo mb_strimwidth($row['nama_event'], 0, 40, "...") ?></td>
              <td><?php echo mb_strimwidth($row['kategori'], 0, 11, "...") ?></td>
              <td><?php echo mb_strimwidth($row['keterangan_event'], 0, 20, "...") ?></td>
              <td><?php echo $row['tanggal_event'] ?></td>
              <td><?php echo mb_strimwidth($row['lokasi_event'], 0, 13, "...") ?></td>
              <td><img src="../<?php echo $row['foto'] ?>" class="circle" alt="" draggable="false"></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
        <ul class="pagination">
          <li class="disabled"><a href="#!"><i class="material-icons">keyboard_arrow_left</i></a></li>
          <li class="active"><a href="#1">1</a></li>
          <li><a href="#2">2</a></li>
          <li><a href="#3">3</a></li>
          <li><a href="#4">4</a></li>
          <li><a href="#!"><i class="material-icons">keyboard_arrow_right</i></a></li>
        </ul>
      </div>
    </main>
    <?php //require_once 'components/footer.php'; ?>
    <script src="../assets/js/jquery.min.js" charset="utf-8"></script>
    <script src="../assets/js/admin.js" charset="utf-8"></script>
  </body>
</html>
