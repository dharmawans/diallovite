<?php
require_once '../config/init.php';
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Diallovite, a free event organizer made by Technosoft">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Create Events</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="tools/admin.css">
    <link href='http://fonts.googleapis.com/css?family=Ubuntu:bold' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
  </head>
  <body>
    <section class="warning_on_smarphone">
      <p>Admin content cannot be open on smartphone</p>
    </section>
    <?php require_once 'components/sidebar.php'; ?>
    <?php require_once 'components/nav.php'; ?>
    <main>
      <div class="container">
        <form action="" method="post">
          <div class="input-field">
            <input type="text" name="" placeholder="Nama event" required>
            <input type="text" name="" placeholder="Kategori event" required>
          </div>
          <div class="input-field">
            <input type="time" name="" placeholder="Waktu event" required>
            <input type="date" name="" placeholder="Jam event" required>
          </div>
          <div class="input-field">
            <textarea name="" rows="9" placeholder="Keterangan event" required></textarea>
          </div>
          <div class="input-field">
            <textarea name="lokasi" rows="9" placeholder="Lokasi event" required></textarea>
          </div>
          <div class="input-field">
            <label for="file" class="input-label"><i class="material-icons">file_upload</i> <span id="label_span">Upload photo</span></label>
            <input type="file" name="file" id="file" class="input-file" multiple/>
            <button type="submit"><span>Buat event</span> <i class="material-icons">send</i></button>
          </div>
        </form>
      </div>
    </main>
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/admin.js" charset="utf-8"></script>
    <?php require_once 'components/footer.php'; ?>
  </body>
</html>
