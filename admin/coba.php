<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
    <style media="screen">
    * {
      box-sizing: border-box;
    }
    *:not(.aside) {
      color: white;
    }
    body {
      margin: 0;
    }
      header {
        width: 100%;
        height: 50px;
        background-color: green;
      }
      aside {
        width: 20%;
        float: left;
        background-color: red;
      }
      main {
        width: 80%;
        float: left;
        background-color: yellow;
      }
      footer {
        height: 50px;
        width: 100%;
        background-color: blue;
      }
    </style>
  </head>
  <body>
    <header>
      blabla
    </header>
    <aside class="aside">
      blibli
    </aside>
    <main>
      blublu
    </main>
    <footer>
      bleble
    </footer>
  </body>
</html>
