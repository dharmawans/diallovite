<aside class="sidebar">
  <ul>
    <li><a href="../index.php"><span>Diallovite</span></a></li>
    <li><a href="home.php"><i class="material-icons">home</i><span>Homepage</span></a></li>
    <li><a href="javascript:void(0)"><i class="material-icons">widgets</i><span>Events Organizer</span> <i class="material-icons multi_menu">keyboard_arrow_right</i></a>
      <ul>
        <li><a href="create.php"><i class="material-icons">event</i><span>Create Events</span></a></li>
        <li><a href="list.php"><i class="material-icons">view_list</i><span>List Events</span></a></li>
      </ul>
    </li>
    <li><a href="peserta.php"><i class="material-icons">people</i><span>Peserta Events</span></a></li>
  </ul>
  <button type="button" class="sidebarBtn" name="button"><i class="material-icons">menu</i></button>
</aside>
