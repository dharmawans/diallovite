<?php
require_once '../config/init.php';
$peserta = tampilkan_peserta();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Diallovite, a free event organizer made by Technosoft">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Peserta</title>
    <link rel="stylesheet" href="tools/admin.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  </head>
  <body>
    <section class="warning_on_smarphone">
      <p>Admin content cannot be open on smartphone</p>
    </section>
    <?php require_once 'components/sidebar.php'; ?>
    <?php require_once 'components/nav.php'; ?>

    <?php require_once 'components/footer.php'; ?>
    <script src="../assets/js/jquery.min.js" charset="utf-8"></script>
    <script src="../assets/js/admin.js" charset="utf-8"></script>
  </body>
</html>
