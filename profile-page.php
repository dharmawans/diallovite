<?php
require_once 'config/init.php';
$id_event = $_GET['id_event'];

if (isset($_GET['id_event'])) {
  $event_per_id = tampilkan_event_per_id($id_event);
  while ($row = mysqli_fetch_assoc($event_per_id)) {
    $id_event = $row['id_event'];
    $nama_organisasi = $row['nama_organisasi'];
    $photo_organisasi = $row['photo_organisasi'];
    $web_organisasi = $row['web_organisasi'];
    $deskripsi_organisasi = $row['deskripsi_organisasi'];
  }
}
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
    <![endif]-->
    <title>Diallovite | Event Organizer Made By Technosoft</title>
    <link rel="shortcut icon" href="assets/images/diallovite_final_latest.ico">
  	<link rel="stylesheet" href="assets/css/materialize.min.css">
    <link rel="stylesheet" href="assets/css/general.css">
    <link rel="stylesheet" href="assets/css/profile.css">
  </head>
  <body>
    <?php require_once 'assets/components/nav/nav-home.php'; ?>
    <main class="mycontainer_without_padding">
      <section id="content-top">
        <div class="profile-picture center-align">
          <img src="<?= $photo_organisasi; ?>" class="circle" width="150">
          <h4 class="font-bold font-cabin capitalize"><?= $nama_organisasi; ?></h4>
          <a href="<?= $web_organisasi; ?>"><?= $web_organisasi; ?></a>
        </div>
        <div class="profile-description">
          <div class="row">
            <div class="col s12 center-align">
              <p class="font-cabin black-text capitalize">
                <?= $deskripsi_organisasi; ?>
              </p>
            </div>
          </div>
        </div>
      </section>
      <section id="content-bottom">
        <div class="other-event">
          <div class="section">
            <p class="grey-text text-darken-3 font-cabin capitalize">Event From <?= $nama_organisasi; ?></p>
            <div class="divider"></div>
          </div>
          <div class="row">
            <div class="col s4">
                <div class="card z-depth-1 small">
                <div class="card-image">
                  <img src="assets/images/sang_idola_cinta.jpg" class="activator" id="image">
                </div>
                <div class="card-content">
                  <a href="javascript:void(0);">Lokasi : </a>
                </div>
                <div class="card-action">
                  <a href="javascript:void(0);">
                    21 - 11 - 2018
                  </a>
                  <a class="right margin-right_reset" href="read_more.php?id_event=<?= $row['id_event']; ?>">
                    Join Event
                  </a>
                </div>
              </div>
            </div>
            <div class="col s4">
                <div class="card z-depth-1 small">
                <div class="card-image">
                  <img src="assets/images/puncak.jpg" class="activator" id="image">
                </div>
                <div class="card-content">
                  <a href="javascript:void(0);">Lokasi : </a>
                </div>
                <div class="card-action">
                  <a href="javascript:void(0);">
                    21 - 11 - 2018
                  </a>
                  <a class="right margin-right_reset" href="read_more.php?id_event=<?= $row['id_event']; ?>">
                    Join Event
                  </a>
                </div>
              </div>
            </div>
            <div class="col s4">
                <div class="card z-depth-1 small">
                <div class="card-image">
                  <img src="assets/images/event4.jpg" class="activator" id="image">
                </div>
                <div class="card-content">
                  <a href="javascript:void(0);">Lokasi : </a>
                </div>
                <div class="card-action">
                  <a href="javascript:void(0);">
                    21 - 11 - 2018
                  </a>
                  <a class="right margin-right_reset" href="read_more.php?id_event=<?= $row['id_event']; ?>">
                    Join Event
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
    <?php require_once 'assets/components/footer/footer.php'; ?>
    <script src="assets/js/jquery.min.js" charset="utf-8"></script>
    <script src="assets/js/materialize.min.js" charset="utf-8"></script>
  </body>
</html>
